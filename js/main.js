$(function(){
    // window loading
    $(window).on('load',function(){
        $('.spinner').fadeOut(500);
        $('.lodeing').fadeOut(1000);
        $(this).remove(1000,$('body').css('overflow','auto'));
    });
    // create animate window 
    $('.navbar-light .navbar-nav .nav-link').on('click',function(e){
        e.preventDefault();
        $('html,body').animate({
            scrollTop: $($(this).data('target')).offset().top + 1
        },1000);

    });
    ///
    $('.what_you_get .card .card-header ul li a').on('click',function(e){
        e.preventDefault();
        $(this).parents('.container').find('img').fadeOut(0).attr('src',$(this).parent('li').data('src')).fadeIn(1000);
        $(this).addClass('active').parent('li').siblings().find('a').removeClass('active');
        $($(this).data('targetbox')).fadeIn(2000).siblings().fadeOut(0);
    });
    //////////////////////////
    // nav Bar scroll
    $(window).on('scroll',function(){
        if($(this).scrollTop() < '800'){
            $('nav').addClass('nav_opacity');
        }else if($(this).scrollTop() > '900'){
            $('nav').addClass('fixed-top');
            $('nav').removeClass('nav_opacity');
        }else{
            $('nav').removeClass('fixed-top nav_opacity');
        }
    }); 
    ///
    var the_heading_text = $('.my_screen .type_heading').data('text'),
        the_heading_length = the_heading_text.length,

        the_span_text = $('.my_screen .type_span').data('text'),
        the_span_length = the_span_text.length,
        n1 = 0,
        n2 = 0;

        //console.log(the_length);
        theType_span = setInterval(function(){
            $('.type_span').each(function(){
                $(this).html($(this).html()+the_span_text[n2])
            });
            n2++;
            if(n2 >= the_span_length){
                clearInterval(theType_span);
            }
        },100);

        theType = setInterval(function(){
            $('.type_heading').each(function(){
                $(this).html($(this).html()+the_heading_text[n1])
            });
            n1++;
            if(n1 >= the_heading_length){
                clearInterval(theType);
            }
        },100);
        ///////////////////////////////
        $('.form_result').blur(function(){
            if( $(this).val() == '' ){
                 $(this).animate({
                     left: '+=30px',
                 },200).animate({
                     border:'1px solid #d85a5a'
                 },1000).animate({
                     left:'-=30px',
                     border:'0'
                 },100);
                 $(this).css({'border':'2px solid #d85a5a'});
            }
            console.log('Go');
        });
        /////////////////////////////////
        //select The Height Box
       var height_box = 0;
        $('.About_Us_boox').each(function(){
            if($(this).height() > height_box){
                height_box =  $(this).height();
            }
        });
        $('.About_Us_boox').height(height_box);
        //////////////////////////////////////
        // animate
        $('.moveClick').on('click',function(){
            $(this).animate({
                left:'+=150px'
            },100).animate({
                left:'-=150px'
            },100).delay(200);
            $(this).animate({
                top:'-=20px',
            },100,function(){
                $(this).animate({
                    top:'+=20px'
                })
            });
        });
        // register_false
        $('.register_false').on('click',function(){
            $(this).animate({
                top:'+=100px'
            },100).animate({
                top:'-=100px'
            },100).delay(200);
            $(this).animate({
                top:'+=100px',
            },100,function(){
                $(this).animate({
                    top:'-=100px'
                })
            });
            $(this).find('.footer_plan_box button').fadeOut(0)
            .fadeIn(1000).css({'backgroundColor':'#d85a5a','border':0});
        });
        //////////////////////////////////////

})